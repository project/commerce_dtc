<?php
/**
 * @file
 */

define('COMMERCE_DTC_ONE_OPTION', 'commerce_dtc_one_option');

/**
 * Implements hook_menu().
 */
function commerce_dtc_menu() {
  $items = array();

  // Direct to cart
  $items['node/%node/dtc'] = array(
    'title' => 'Add product(s) directly to the cart',
    'description' => 'Based on querystring parameters, add products directly to the cart.',
    'page callback' => 'commerce_dtc_add_to_cart',
    'page arguments' => array(1),
    'access arguments' => array('access content'),
    'file' => 'includes/commerce_dtc.pages.inc',
  );

  // Configuration
  $items['admin/commerce/config/commerce_dtc'] = array(
    'title' => 'Direct to Cart Querystring Mapping',
    'description' => 'Map product attribute field names to custom values that will read from the URL.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('commerce_dtc_settings_form'),
    'access arguments' => array('configure store'),
    'file' => 'includes/commerce_dtc.admin.inc',
  );

  return $items;
}

/**
 * Helper function: Generate / lookup short attribute name.
 *
 * Field names can be long and not user friendly when used in a querystring to
 * set an attribute field value. This function strips 'field_' from the field
 * name by default. This turns 'field_color' into 'color'.
 *
 * @param  string $field_name
 *
 * @return string
 *   Shortened field name.
 */
function _commerce_dtc_field_shortname($field_name) {
  $defaults = module_exists('commerce_pdas') ? variable_get('commerce_pdas_short_names', array()) : array();

  $short_names = variable_get('commerce_dtc_short_names', $defaults);
  return !empty($short_names[$field_name]) ? $short_names[$field_name] : str_replace('field_', '', $field_name);
}

/**
 * Returns an array of product fields enabled as an attribute field
 * for Add to Cart forms from provided products.
 *
 * Note: taken mostly from _commerce_product_urls_get_cart_attribute_fields()
 */
function _commerce_dtc_get_cart_attribute_fields() {
  $attribute_fields = &drupal_static(__FUNCTION__);

  if (isset($attribute_fields)) {
    return $attribute_fields;
  }

  $product_types = commerce_product_types();

  // Now let's loop through all product types and generate an array
  // of all possible attribute fields available in them. Quite a big part
  // of this code is borrowed from commerce_cart_add_to_cart_form().
  $attribute_fields = array();

  foreach ($product_types as $product_type => $product_type_info) {

    foreach (field_info_instances('commerce_product', $product_type) as $field_name => $instance) {
      // A field qualifies if it is single value, required and uses a widget
      // with a definite set of options. For the sake of simplicity, this is
      // currently restricted to fields defined by the options module.
      $field = field_info_field($instance['field_name']);

      // Get the array of Cart settings pertaining to this instance.
      $commerce_cart_settings = commerce_cart_field_instance_attribute_settings($instance);

      // If the instance is of a field type that is eligible to function as
      // a product attribute field and if its attribute field settings
      // specify that this functionality is enabled...
      if (commerce_cart_field_attribute_eligible($field) && $commerce_cart_settings['attribute_field']) {

        // Get the options properties from the options module and store the
        // options for the instance in select list format in the array of
        // qualifying fields.
        $properties = _options_properties('select', FALSE, TRUE, TRUE);

        // Try to fetch localized names.
        $allowed_values = NULL;

        // Prepare translated options if using the i18n_field module.
        if (module_exists('i18n_field')) {
          if (($translate = i18n_field_type_info($field['type'], 'translate_options'))) {
            $allowed_values = $translate($field);
            _options_prepare_options($allowed_values, $properties);
          }

          // Translate the field title if set.
          if (!empty($instance['label'])) {
            $instance['label'] = i18n_field_translate_property($instance, 'label');
          }
        }

        // Otherwise just use the base language values.
        if (empty($allowed_values)) {
          $allowed_values = _options_get_options($field, $instance, $properties, 'commerce_product', array());
        }

        if (!empty($allowed_values)) {
          $attribute_fields[$field_name]['options'] = $allowed_values;
          $attribute_fields[$field_name]['label'] = $instance['label'];
          $attribute_fields[$field_name]['product_types'][$product_type] = $product_type;
        }

      }
    }
  }

  return $attribute_fields;
}
